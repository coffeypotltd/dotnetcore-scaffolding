﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreScaffolding.Models.Configuration
{
    public class ClientConfiguration
    {
        public Client[] Clients { get; set; }
    }
}
