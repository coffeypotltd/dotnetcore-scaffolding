﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CoreScaffolding
{
    public class Program
    {
        private static readonly ILogger<Program> _logger;

        static Program()
        {
            ServiceProvider services = new ServiceCollection().AddLogging(x => x.SetMinimumLevel(LogLevel.Debug)).BuildServiceProvider();
            _logger = services.GetService<ILoggerFactory>().AddLog4Net().CreateLogger<Program>();
        }

        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
