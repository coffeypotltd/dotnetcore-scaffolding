﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreScaffolding.Logging
{
    public interface ILogger
    {
        void LogInfo(string message);
        void LogError(string message);
        void LogCritical(string message);
    }
}
