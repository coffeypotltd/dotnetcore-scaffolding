﻿using log4net;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreScaffolding.Logging
{
    public class Logger : ILogger
    {
        public readonly ILog _log;
        public Logger()
        {
            _log = LogManager.GetLogger(typeof(ControllerContext));
        }


        public void LogInfo(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                _log.Info(message);
            }
            else
            {
                _log.Info("Cannot log blamk message! But an error has occurred.");
            }
        }

        public void LogError(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                _log.Error(message);
            }
            else
            {
                _log.Error("Cannot log blamk message! But an error has occurred.");
            }
        }

        public void LogCritical(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                _log.Info(message);
            }
            else
            {
                _log.Fatal("Cannot log blamk message! But an error has occurred.");
            }
        }
    }
}
