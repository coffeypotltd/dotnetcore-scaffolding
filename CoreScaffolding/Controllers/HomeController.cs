﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoreScaffolding.Models;
using Microsoft.Extensions.Options;
using CoreScaffolding.Models.Configuration;
using log4net;
using CoreScaffolding.Logging;

namespace CoreScaffolding.Controllers
{
    public class HomeController : Controller
    {
        private readonly IOptions<ClientConfiguration> _config;
        private readonly ILogger _logger;

        public HomeController(IOptions<ClientConfiguration> config, ILogger logger)
        {
            _config = config;
            _logger = logger;
            _logger.LogCritical("File is too stronk. Log dis");
        }

        public IActionResult Index()
        {
            var s = _config.Value;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
