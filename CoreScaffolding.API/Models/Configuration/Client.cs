﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreScaffolding.API.Models.Configuration
{
    public class Client
    {
        public string ClientName { get; set; }
        public string ClientID { get; set; }
    }
}
